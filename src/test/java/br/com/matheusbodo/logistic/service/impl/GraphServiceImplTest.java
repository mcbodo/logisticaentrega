package br.com.matheusbodo.logistic.service.impl;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.matheusbodo.logistic.algorithm.DijkstraAlgorithm;
import br.com.matheusbodo.logistic.dto.BestRouteRequest;
import br.com.matheusbodo.logistic.dto.BestRouteResponse;
import br.com.matheusbodo.logistic.dto.Result;
import br.com.matheusbodo.logistic.entity.Edge;
import br.com.matheusbodo.logistic.entity.Graph;
import br.com.matheusbodo.logistic.entity.Vertex;
import br.com.matheusbodo.logistic.repository.GraphRepository;
import br.com.matheusbodo.logistic.service.GraphCacheService;
import br.com.matheusbodo.logistic.service.MessageService;

public class GraphServiceImplTest {

	private static final BigDecimal DISTANCE = new BigDecimal(10);
	private static final String DESTINATION_NAME = "B";
	private static final String SOURCE_NAME = "A";
	private static final String GRAPH_NAME = "SP";
	private static final String MESSAGE = "message";
	private static final BigDecimal FUEL_COMSUMPTION = new BigDecimal("10");
	private static final BigDecimal FUEL_PRICE = new BigDecimal("2.50");
	private static final String BEST_ROUTE = "A B";
	private static final BigDecimal COST = new BigDecimal("2.50");
	private GraphServiceImpl service;
	
	@Before
	public void setUp() {
		service = new GraphServiceImpl();
	}
	
	@Test
	public void testCreate() throws Exception {
		Graph graph = createGraph();
	
		GraphRepository graphRepositoryMock = Mockito.mock(GraphRepository.class);
		Mockito.when(graphRepositoryMock.findByName(GRAPH_NAME)).thenReturn(null);
		service.setGraphRepository(graphRepositoryMock);
		
		Result<Graph> result = service.create(graph);
		
		Assert.assertTrue(result.isSuccess());
		Mockito.verify(graphRepositoryMock).save(graph);
	}
	
	@Test
	public void testCreateWithRepeatedName() throws Exception {
		Graph graph = createGraph();
	
		GraphRepository graphRepositoryMock = Mockito.mock(GraphRepository.class);
		Mockito.when(graphRepositoryMock.findByName(GRAPH_NAME)).thenReturn(new Graph());
		service.setGraphRepository(graphRepositoryMock);
		
		MessageService messageServiceMock = Mockito.mock(MessageService.class);
		Mockito.when(messageServiceMock.getMessage("graph.name.already.exists")).thenReturn(MESSAGE);
		service.setMessageService(messageServiceMock);
		
		Result<Graph> result = service.create(graph);
		
		Assert.assertFalse(result.isSuccess());
		Assert.assertEquals("name_exists", result.getCode());
		Assert.assertEquals(MESSAGE, result.getMessage());
	}
	
	@Test
	public void testCalculateBestRouteUsingCache() throws Exception {
		BestRouteRequest request = createBestRouteRequest();
		
		GraphCacheService cacheServiceMock = Mockito.mock(GraphCacheService.class);
		Mockito.when(cacheServiceMock.getBestRoute(GRAPH_NAME, SOURCE_NAME, DESTINATION_NAME)).thenReturn(BEST_ROUTE);
		Mockito.when(cacheServiceMock.getBestRouteDistance(GRAPH_NAME, SOURCE_NAME, DESTINATION_NAME)).thenReturn(DISTANCE);
		service.setGraphCacheService(cacheServiceMock);
		
		Result<BestRouteResponse> result = service.calculateBestRoute(request);
		
		Assert.assertTrue(result.isSuccess());
		Assert.assertEquals(BEST_ROUTE, result.getObject().getRoute());
		Assert.assertEquals(COST, result.getObject().getCost());
	}
	
	@Test
	public void testCalculateBestRoute() throws Exception {
		Graph graph = createGraph();
		
		BestRouteRequest request = createBestRouteRequest();
		
		GraphCacheService cacheServiceMock = Mockito.mock(GraphCacheService.class);
		Mockito.when(cacheServiceMock.getBestRoute(GRAPH_NAME, SOURCE_NAME, DESTINATION_NAME)).thenReturn(null);
		Mockito.when(cacheServiceMock.getBestRouteDistance(GRAPH_NAME, SOURCE_NAME, DESTINATION_NAME)).thenReturn(null);
		service.setGraphCacheService(cacheServiceMock);
		
		GraphRepository graphRepositoryMock = Mockito.mock(GraphRepository.class);
		Mockito.when(graphRepositoryMock.findByName(GRAPH_NAME)).thenReturn(graph);
		service.setGraphRepository(graphRepositoryMock);
		
		Result<BestRouteResponse> result = service.calculateBestRoute(request);
		
		Assert.assertTrue(result.isSuccess());
		Assert.assertEquals(BEST_ROUTE, result.getObject().getRoute());
		Assert.assertEquals(COST, result.getObject().getCost());
		Mockito.verify(cacheServiceMock).updateCache(Mockito.eq(graph), Mockito.eq(graph.getVertex(SOURCE_NAME)), Mockito.any(DijkstraAlgorithm.class));
	}
	
	@Test
	public void testCalculateBestRouteGraphNotFound() throws Exception {
		BestRouteRequest request = createBestRouteRequest();
		
		GraphCacheService cacheServiceMock = Mockito.mock(GraphCacheService.class);
		Mockito.when(cacheServiceMock.getBestRoute(GRAPH_NAME, SOURCE_NAME, DESTINATION_NAME)).thenReturn(null);
		Mockito.when(cacheServiceMock.getBestRouteDistance(GRAPH_NAME, SOURCE_NAME, DESTINATION_NAME)).thenReturn(null);
		service.setGraphCacheService(cacheServiceMock);
		
		GraphRepository graphRepositoryMock = Mockito.mock(GraphRepository.class);
		Mockito.when(graphRepositoryMock.findByName(GRAPH_NAME)).thenReturn(null);
		service.setGraphRepository(graphRepositoryMock);
		
		MessageService messageServiceMock = Mockito.mock(MessageService.class);
		Mockito.when(messageServiceMock.getMessage("graph.not.found")).thenReturn(MESSAGE);
		service.setMessageService(messageServiceMock);
		
		Result<BestRouteResponse> result = service.calculateBestRoute(request);
		
		Assert.assertFalse(result.isSuccess());
		Assert.assertEquals("graph_not_found", result.getCode());
		Assert.assertEquals(MESSAGE, result.getMessage());
	}
	
	@Test
	public void testCalculateBestRouteSourceNotFound() throws Exception {
		Graph graph = new Graph();
		
		BestRouteRequest request = createBestRouteRequest();
		
		GraphCacheService cacheServiceMock = Mockito.mock(GraphCacheService.class);
		Mockito.when(cacheServiceMock.getBestRoute(GRAPH_NAME, SOURCE_NAME, DESTINATION_NAME)).thenReturn(null);
		Mockito.when(cacheServiceMock.getBestRouteDistance(GRAPH_NAME, SOURCE_NAME, DESTINATION_NAME)).thenReturn(null);
		service.setGraphCacheService(cacheServiceMock);
		
		GraphRepository graphRepositoryMock = Mockito.mock(GraphRepository.class);
		Mockito.when(graphRepositoryMock.findByName(GRAPH_NAME)).thenReturn(graph);
		service.setGraphRepository(graphRepositoryMock);
		
		MessageService messageServiceMock = Mockito.mock(MessageService.class);
		Mockito.when(messageServiceMock.getMessage("source.not.found")).thenReturn(MESSAGE);
		service.setMessageService(messageServiceMock);
		
		Result<BestRouteResponse> result = service.calculateBestRoute(request);
		
		Assert.assertFalse(result.isSuccess());
		Assert.assertEquals("source_not_found", result.getCode());
		Assert.assertEquals(MESSAGE, result.getMessage());
	}
	
	@Test
	public void testCalculateBestRouteDestinationNotFound() throws Exception {
		Graph graph = new Graph();
		graph.addVertex(new Vertex(graph, SOURCE_NAME));
		
		BestRouteRequest request = createBestRouteRequest();
		
		GraphCacheService cacheServiceMock = Mockito.mock(GraphCacheService.class);
		Mockito.when(cacheServiceMock.getBestRoute(GRAPH_NAME, SOURCE_NAME, DESTINATION_NAME)).thenReturn(null);
		Mockito.when(cacheServiceMock.getBestRouteDistance(GRAPH_NAME, SOURCE_NAME, DESTINATION_NAME)).thenReturn(null);
		service.setGraphCacheService(cacheServiceMock);
		
		GraphRepository graphRepositoryMock = Mockito.mock(GraphRepository.class);
		Mockito.when(graphRepositoryMock.findByName(GRAPH_NAME)).thenReturn(graph);
		service.setGraphRepository(graphRepositoryMock);
		
		MessageService messageServiceMock = Mockito.mock(MessageService.class);
		Mockito.when(messageServiceMock.getMessage("destination.not.found")).thenReturn(MESSAGE);
		service.setMessageService(messageServiceMock);
		
		Result<BestRouteResponse> result = service.calculateBestRoute(request);
		
		Assert.assertFalse(result.isSuccess());
		Assert.assertEquals("destination_not_found", result.getCode());
		Assert.assertEquals(MESSAGE, result.getMessage());
	}
	
	@Test
	public void testCalculateBestRoutePathNotFound() throws Exception {
		Graph graph = createGraph();
		
		BestRouteRequest request = createBestRouteRequest();
		request.setSourceName(DESTINATION_NAME);
		request.setDestinationName(SOURCE_NAME);
		
		GraphCacheService cacheServiceMock = Mockito.mock(GraphCacheService.class);
		Mockito.when(cacheServiceMock.getBestRoute(GRAPH_NAME, SOURCE_NAME, DESTINATION_NAME)).thenReturn(null);
		Mockito.when(cacheServiceMock.getBestRouteDistance(GRAPH_NAME, SOURCE_NAME, DESTINATION_NAME)).thenReturn(null);
		service.setGraphCacheService(cacheServiceMock);
		
		GraphRepository graphRepositoryMock = Mockito.mock(GraphRepository.class);
		Mockito.when(graphRepositoryMock.findByName(GRAPH_NAME)).thenReturn(graph);
		service.setGraphRepository(graphRepositoryMock);
		
		MessageService messageServiceMock = Mockito.mock(MessageService.class);
		Mockito.when(messageServiceMock.getMessage("path.not.found")).thenReturn(MESSAGE);
		service.setMessageService(messageServiceMock);
		
		Result<BestRouteResponse> result = service.calculateBestRoute(request);
		
		Assert.assertFalse(result.isSuccess());
		Assert.assertEquals("path_not_found", result.getCode());
		Assert.assertEquals(MESSAGE, result.getMessage());
	}
	
	private BestRouteRequest createBestRouteRequest() {
		BestRouteRequest request = new BestRouteRequest();
		request.setGraphName(GRAPH_NAME);
		request.setSourceName(SOURCE_NAME);
		request.setDestinationName(DESTINATION_NAME);
		request.setFuelConsumption(FUEL_COMSUMPTION);
		request.setFuelPrice(FUEL_PRICE);
		return request;
	}

	private Graph createGraph() {
		Graph graph = new Graph();
		graph.setName(GRAPH_NAME);
		Vertex vertexA = new Vertex(graph, SOURCE_NAME);
		Vertex vertexB = new Vertex(graph, DESTINATION_NAME);
		
		graph.addVertex(vertexA);
		graph.addVertex(vertexB);
		
		graph.addEdge(new Edge(graph, vertexA, vertexB, DISTANCE));
		return graph;
	}
}
