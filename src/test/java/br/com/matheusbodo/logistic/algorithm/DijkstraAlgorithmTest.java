package br.com.matheusbodo.logistic.algorithm;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.matheusbodo.logistic.entity.Edge;
import br.com.matheusbodo.logistic.entity.Graph;
import br.com.matheusbodo.logistic.entity.Vertex;

public class DijkstraAlgorithmTest {

	@Test
	public void testExecute() {
		Graph graph = new Graph();
		  
		for (int i = 0; i < 11; i++) {
			Vertex location = new Vertex(graph, "Node_" + i);
			graph.addVertex(location);
		}
		List<Vertex> nodes = graph.getVertexes();

		graph.addEdge(new Edge(graph, nodes.get(0), nodes.get(1), new BigDecimal(85)));
		graph.addEdge(new Edge(graph, nodes.get(0), nodes.get(2), new BigDecimal(217)));
		graph.addEdge(new Edge(graph, nodes.get(0), nodes.get(4), new BigDecimal(173)));
		graph.addEdge(new Edge(graph, nodes.get(2), nodes.get(6), new BigDecimal(186)));
		graph.addEdge(new Edge(graph, nodes.get(2), nodes.get(7), new BigDecimal(103)));
		graph.addEdge(new Edge(graph, nodes.get(3), nodes.get(7), new BigDecimal(183)));
		graph.addEdge(new Edge(graph, nodes.get(5), nodes.get(8), new BigDecimal(250)));
		graph.addEdge(new Edge(graph, nodes.get(8), nodes.get(9), new BigDecimal(84)));
		graph.addEdge(new Edge(graph, nodes.get(7), nodes.get(9), new BigDecimal(167)));
		graph.addEdge(new Edge(graph, nodes.get(4), nodes.get(9), new BigDecimal(502)));
		graph.addEdge(new Edge(graph, nodes.get(9), nodes.get(10), new BigDecimal(40)));
		graph.addEdge(new Edge(graph, nodes.get(1), nodes.get(10), new BigDecimal(600)));

		// Lets check from location Loc_0 to Loc_10
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
		dijkstra.execute(nodes.get(0));
		LinkedList<Vertex> path = dijkstra.getPath(nodes.get(10));
		
		Assert.assertArrayEquals(new Object[] {nodes.get(0), nodes.get(2), nodes.get(7), nodes.get(9), nodes.get(10)} , path.toArray());
		Assert.assertEquals(new BigDecimal(527), dijkstra.getShortestDistance(nodes.get(10)));
	}
	
	@Test
	public void testExcuteExerciseSample() {
		Graph graph = new Graph();
		  
		for (int i = 0; i < 5; i++) {
			Vertex location = new Vertex(graph, "Node_" + i);
			graph.addVertex(location);
		}
		List<Vertex> nodes = graph.getVertexes();

		graph.addEdge(new Edge(graph, nodes.get(0), nodes.get(1), new BigDecimal(10)));
		graph.addEdge(new Edge(graph, nodes.get(1), nodes.get(3), new BigDecimal(15)));
		graph.addEdge(new Edge(graph, nodes.get(0), nodes.get(2), new BigDecimal(20)));
		graph.addEdge(new Edge(graph, nodes.get(2), nodes.get(3), new BigDecimal(30)));
		graph.addEdge(new Edge(graph, nodes.get(1), nodes.get(4), new BigDecimal(50)));
		graph.addEdge(new Edge(graph, nodes.get(3), nodes.get(4), new BigDecimal(30)));

		// Lets check from location Loc_0 to Loc_3
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
		dijkstra.execute(nodes.get(0));
		LinkedList<Vertex> path = dijkstra.getPath(nodes.get(3));
		
		Assert.assertArrayEquals(new Object[] {nodes.get(0), nodes.get(1), nodes.get(3)} , path.toArray());
		Assert.assertEquals(new BigDecimal(25), dijkstra.getShortestDistance(nodes.get(3)));
	}
	
	@Test
	public void testExcuteExerciseSampleVariation() {
		Graph graph = new Graph();
		  
		for (int i = 0; i < 5; i++) {
			Vertex location = new Vertex(graph, "Node_" + i);
			graph.addVertex(location);
		}
		
		List<Vertex> nodes = graph.getVertexes();

		graph.addEdge(new Edge(graph, nodes.get(0), nodes.get(1), new BigDecimal(10)));
		graph.addEdge(new Edge(graph, nodes.get(1), nodes.get(3), new BigDecimal(15)));
		graph.addEdge(new Edge(graph, nodes.get(0), nodes.get(2), new BigDecimal(20)));
		graph.addEdge(new Edge(graph, nodes.get(2), nodes.get(3), new BigDecimal(30)));
		graph.addEdge(new Edge(graph, nodes.get(1), nodes.get(4), new BigDecimal(50)));
		graph.addEdge(new Edge(graph, nodes.get(3), nodes.get(4), new BigDecimal(30)));

		// Lets check from location Loc_1 to Loc_4
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
		dijkstra.execute(nodes.get(1));
		LinkedList<Vertex> path = dijkstra.getPath(nodes.get(4));
		
		Assert.assertArrayEquals(new Object[] {nodes.get(1), nodes.get(3), nodes.get(4)} , path.toArray());
		Assert.assertEquals(new BigDecimal(45), dijkstra.getShortestDistance(nodes.get(4)));
	}
	
	@Test
	public void testImpossiblePath() {
		Graph graph = new Graph();
		  
		for (int i = 0; i < 5; i++) {
			Vertex location = new Vertex(graph, "Node_" + i);
			graph.addVertex(location);
		}
		
		List<Vertex> nodes = graph.getVertexes();

		graph.addEdge(new Edge(graph, nodes.get(0), nodes.get(1), new BigDecimal(10)));
		graph.addEdge(new Edge(graph, nodes.get(1), nodes.get(3), new BigDecimal(15)));
		graph.addEdge(new Edge(graph, nodes.get(0), nodes.get(2), new BigDecimal(20)));
		graph.addEdge(new Edge(graph, nodes.get(2), nodes.get(3), new BigDecimal(30)));
		graph.addEdge(new Edge(graph, nodes.get(1), nodes.get(4), new BigDecimal(50)));
		graph.addEdge(new Edge(graph, nodes.get(3), nodes.get(4), new BigDecimal(30)));

		// Lets check from location Loc_1 to Loc_4
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
		dijkstra.execute(nodes.get(1));
		LinkedList<Vertex> path = dijkstra.getPath(nodes.get(2));
		
		Assert.assertNull(path);
	}

}
