package br.com.matheusbodo.logistic.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;

import br.com.matheusbodo.logistic.Application;
import br.com.matheusbodo.logistic.dto.BestRouteRequest;
import br.com.matheusbodo.logistic.dto.BestRouteResponse;
import br.com.matheusbodo.logistic.dto.GraphDTO;
import br.com.matheusbodo.logistic.repository.GraphRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class BestRouteControllerTest {
	
	private static final String GRAPH_NAME = "SP";

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private GraphRepository graphRepository;

	@Before
	public void setup() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		graphRepository.deleteAll();
	}

	@Test
	public void calculateBestRute() throws Exception {
		createGraph();
		BestRouteRequest request = createBestRouteRequest();
		Gson gson = new Gson();
		String jsonRequest = gson.toJson(request);
		
		BestRouteResponse response = createBestRouteResponse();
		String jsonResponse = gson.toJson(response);
		mockMvc.perform(MockMvcRequestBuilders.post("/best-route/calculate")
				.content(jsonRequest)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string(jsonResponse));
	}

	private BestRouteRequest createBestRouteRequest() {
		BestRouteRequest request = new BestRouteRequest();
		request.setGraphName(GRAPH_NAME);
		request.setSourceName("A");
		request.setDestinationName("C");
		request.setFuelConsumption(BigDecimal.valueOf(10));
		request.setFuelPrice(BigDecimal.valueOf(2.5));
		return request;
	}
	
	private BestRouteResponse createBestRouteResponse() {
		BestRouteResponse response = new BestRouteResponse();
		response.setRoute("A B C");
		response.setCost(BigDecimal.valueOf(7.5).setScale(2));
		return response;
	}

	private void createGraph() {
		GraphDTO graphRequest = new GraphDTO();
		graphRequest.setName(GRAPH_NAME);
		
		List<String> routes = new ArrayList<>();
		routes.add("A B 10");
		routes.add("B C 20");
		routes.add("A C 250");
		graphRequest.setRoutes(routes);
		
		graphRepository.save(graphRequest.buildGraph());
	}


}
