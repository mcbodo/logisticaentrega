package br.com.matheusbodo.logistic.controller;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.StartsWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;

import br.com.matheusbodo.logistic.Application;
import br.com.matheusbodo.logistic.dto.GraphDTO;
import br.com.matheusbodo.logistic.entity.Graph;
import br.com.matheusbodo.logistic.repository.GraphRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class GraphControllerTest {

	private static final String GRAPH_NAME = "SP";

	private MockMvc mockMvc;
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Autowired
	private GraphRepository graphRepository;
	
	@Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        graphRepository.deleteAll();
    }
	
	@Test
    public void createGraph() throws Exception {
		GraphDTO graphRequest = createGraphDTO();
		
		String json = new Gson().toJson(graphRequest);
        Matcher<String> matcher = new StartsWith("/graphs/");
		mockMvc.perform(MockMvcRequestBuilders.post("/graphs")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.header().string("Location", matcher));
    }
	
	@Test
    public void createGraphInvalidRequest() throws Exception {
		GraphDTO graphRequest = new GraphDTO();
		String json = new Gson().toJson(graphRequest);
		mockMvc.perform(MockMvcRequestBuilders.post("/graphs")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
	
	@Test
    public void createGraphDuplicated() throws Exception {
		GraphDTO graphRequest = createGraphDTO();
		
		Graph graph = graphRequest.buildGraph();
		graphRepository.save(graph);
		
		String json = new Gson().toJson(graphRequest);
		mockMvc.perform(MockMvcRequestBuilders.post("/graphs")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
	
	@Test
    public void findGraph() throws Exception {
		GraphDTO graphResponse = createGraphDTO();
		
		Graph graph = graphResponse.buildGraph();
		graphRepository.save(graph);
		
		graphResponse.setId(graph.getId());
		String json = new Gson().toJson(graphResponse);
		
        mockMvc.perform(MockMvcRequestBuilders.get("/graphs/" + graph.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(json));
    }
	
	@Test
    public void graphNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/graphs/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
	
	@Test
    public void deleteGraph() throws Exception {
		GraphDTO graphDTO = createGraphDTO();
		Graph graph = graphDTO.buildGraph();
		graphRepository.save(graph);
		
        mockMvc.perform(MockMvcRequestBuilders.delete("/graphs/" + graph.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());
        
        Assert.assertEquals(0, graphRepository.count());
    }
	
	@Test
    public void deleteNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/graphs/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
	
	private GraphDTO createGraphDTO() {
		GraphDTO graphRequest = new GraphDTO();
		graphRequest.setName(GRAPH_NAME);
		
		List<String> routes = new ArrayList<>();
		routes.add("A B 10.00");
		routes.add("B C 20.00");
		routes.add("A C 250.00");
		graphRequest.setRoutes(routes);
		return graphRequest;
	}
	
}
