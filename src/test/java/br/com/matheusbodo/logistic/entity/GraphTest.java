package br.com.matheusbodo.logistic.entity;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class GraphTest {

	private Graph graph;
	
	@Before
	public void setUp() throws Exception{
		graph = new Graph();
	}
	
	@Test
	public void testAddRouteSuccessfully() throws Exception {
		graph.addRoute("A", "B", new BigDecimal(20));
		
		Assert.assertEquals(2, graph.getVertexes().size());
		Assert.assertEquals(1, graph.getEdges().size());
	}
	
	@Test
	public void testAddSourceVertextSuccessfully() throws Exception {
		graph.addRoute("A", "B", new BigDecimal(20));
		
		Assert.assertEquals("A", graph.getVertexes().get(0).getName());
	}
	
	@Test
	public void testAddDestinationVertextSuccessfully() throws Exception {
		graph.addRoute("A", "B", new BigDecimal(20));
		
		Assert.assertEquals("B", graph.getVertexes().get(1).getName());
	}
	
	@Test
	public void testAddEdgeVertextSuccessfully() throws Exception {
		graph.addRoute("A", "B", new BigDecimal(20));
		
		Assert.assertEquals("A", graph.getEdges().get(0).getSource().getName());
		Assert.assertEquals("B", graph.getEdges().get(0).getDestination().getName());
		Assert.assertEquals(new BigDecimal(20), graph.getEdges().get(0).getWeight());
	}
	
	@Test
	public void testAddMultipleRoutesSuccessfully() throws Exception {
		graph.addRoute("A", "B", new BigDecimal(20));
		graph.addRoute("B", "C", new BigDecimal(10));
		graph.addRoute("A", "C", new BigDecimal(40));
		graph.addRoute("C", "D", new BigDecimal(10));
		graph.addRoute("B", "D", new BigDecimal(25));
		
		Assert.assertEquals(4, graph.getVertexes().size());
		Assert.assertEquals(5, graph.getEdges().size());
	}
	
	@Test
	public void testIgnoresRepeatedRoute() throws Exception {
		graph.addRoute("A", "B", new BigDecimal(20));
		graph.addRoute("A", "B", new BigDecimal(30));
		
		Assert.assertEquals(2, graph.getVertexes().size());
		Assert.assertEquals(1, graph.getEdges().size());
		
		Assert.assertEquals(new BigDecimal(20), graph.getEdges().get(0).getWeight());
	}
}
