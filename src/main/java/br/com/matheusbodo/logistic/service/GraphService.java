package br.com.matheusbodo.logistic.service;

import br.com.matheusbodo.logistic.dto.BestRouteRequest;
import br.com.matheusbodo.logistic.dto.BestRouteResponse;
import br.com.matheusbodo.logistic.dto.Result;
import br.com.matheusbodo.logistic.entity.Graph;

public interface GraphService {

	Result<Graph> create(Graph graph);

	Graph findOne(Long id);

	Result<BestRouteResponse> calculateBestRoute(BestRouteRequest request);

	Result<Graph> delete(Long id);

}
