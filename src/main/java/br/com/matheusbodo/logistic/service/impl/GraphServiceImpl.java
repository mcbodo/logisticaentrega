package br.com.matheusbodo.logistic.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.matheusbodo.logistic.algorithm.DijkstraAlgorithm;
import br.com.matheusbodo.logistic.dto.BestRouteRequest;
import br.com.matheusbodo.logistic.dto.BestRouteResponse;
import br.com.matheusbodo.logistic.dto.Result;
import br.com.matheusbodo.logistic.entity.Graph;
import br.com.matheusbodo.logistic.entity.Vertex;
import br.com.matheusbodo.logistic.repository.GraphRepository;
import br.com.matheusbodo.logistic.service.GraphCacheService;
import br.com.matheusbodo.logistic.service.GraphService;
import br.com.matheusbodo.logistic.service.MessageService;

@Service
@Transactional
public class GraphServiceImpl implements GraphService {

	@Autowired
	private GraphRepository graphRepository;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private GraphCacheService graphCacheService;
	
	@Override
	public Result<Graph> create(Graph graph) {
		Graph existingGraph = graphRepository.findByName(graph.getName());
		if (existingGraph != null) {
			return new Result<Graph>(false, "name_exists", messageService.getMessage("graph.name.already.exists"));
		}
		graphRepository.save(graph);
		return new Result<Graph>(true, graph);
	}

	@Override
	public Graph findOne(Long id) {
		return graphRepository.findOne(id);
	}
	
	@Override
	public Result<Graph> delete(Long id) {
		Graph graph = findOne(id);
		if (graph == null) {
			return new Result<Graph>(false);
		}
		graphRepository.delete(graph);
		return new Result<Graph>(true);
	}

	@Override
	public Result<BestRouteResponse> calculateBestRoute(BestRouteRequest request) {
		String graphName = request.getGraphName();
		String sourceName = request.getSourceName();
		String destinationName = request.getDestinationName();
		BestRouteResponse response = new BestRouteResponse();
		
		String bestRoute = graphCacheService.getBestRoute(graphName, sourceName, destinationName);
		BigDecimal distance = graphCacheService.getBestRouteDistance(graphName, sourceName, destinationName);
		if (StringUtils.isNotBlank(bestRoute) && distance != null) {
			response.setRoute(bestRoute);
			response.setCost(calculateTravelCost(request, distance));
			return new Result<BestRouteResponse>(true, response);
		}
		
		Graph graph = graphRepository.findByName(graphName);
		if (graph == null) {
			return new Result<BestRouteResponse>(false, "graph_not_found", messageService.getMessage("graph.not.found"));
		}
		
		Vertex source = graph.getVertex(sourceName);
		if (source == null) {
			return new Result<BestRouteResponse>(false, "source_not_found", messageService.getMessage("source.not.found"));
		}
		
		Vertex destination = graph.getVertex(destinationName);
		if (destination == null) {
			return new Result<BestRouteResponse>(false, "destination_not_found", messageService.getMessage("destination.not.found"));
		}
		
		DijkstraAlgorithm dijkstraAlgorithm = new DijkstraAlgorithm(graph);
		dijkstraAlgorithm.execute(source);
		
		LinkedList<Vertex> path = dijkstraAlgorithm.getPath(destination);
		if (path == null) {
			return new Result<BestRouteResponse>(false, "path_not_found", messageService.getMessage("path.not.found"));
		}
		
		for (Vertex vertex : path) {
			response.addVertex(vertex.getName());
		}
		distance = dijkstraAlgorithm.getShortestDistance(destination);
		response.setCost(calculateTravelCost(request, distance));
		
		graphCacheService.updateCache(graph, source, dijkstraAlgorithm);
		
		return new Result<BestRouteResponse>(true, response);
	}

	private BigDecimal calculateTravelCost(BestRouteRequest request, BigDecimal distance) {
		BigDecimal fuelUsed = BigDecimal.ZERO;
		if (request.getFuelConsumption().compareTo(BigDecimal.ZERO) != 0) {
			fuelUsed = distance.divide(request.getFuelConsumption(), 2, RoundingMode.HALF_UP);
		}
		return fuelUsed.multiply(request.getFuelPrice()).setScale(2);
	}
	
	void setGraphRepository(GraphRepository graphRepository) {
		this.graphRepository = graphRepository;
	}
	
	void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}
	
	void setGraphCacheService(GraphCacheService graphCacheService) {
		this.graphCacheService = graphCacheService;
	}
}
