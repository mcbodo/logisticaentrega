package br.com.matheusbodo.logistic.service;

import java.math.BigDecimal;

import br.com.matheusbodo.logistic.algorithm.DijkstraAlgorithm;
import br.com.matheusbodo.logistic.entity.Graph;
import br.com.matheusbodo.logistic.entity.Vertex;

public interface GraphCacheService {

	void updateCache(Graph graph, Vertex source, DijkstraAlgorithm dijkstraAlgorithm);

	String getBestRoute(String graphName, String sourceName, String destinationName);

	BigDecimal getBestRouteDistance(String graphName, String sourceName, String destinationName);

}
