package br.com.matheusbodo.logistic.service.impl;

import java.math.BigDecimal;
import java.util.LinkedList;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import br.com.matheusbodo.logistic.algorithm.DijkstraAlgorithm;
import br.com.matheusbodo.logistic.entity.Graph;
import br.com.matheusbodo.logistic.entity.Vertex;
import br.com.matheusbodo.logistic.service.GraphCacheService;

@Service
public class GraphCacheServiceImpl implements GraphCacheService {

	private static final String DISTANCE_KEY = "distance";
	private static final String ROUTE_KEY = "route";

	private static final Logger LOGGER = Logger.getLogger(GraphCacheServiceImpl.class);
	
	@Autowired
	private StringRedisTemplate template;

	@Override
	public void updateCache(Graph graph, Vertex source, DijkstraAlgorithm dijkstraAlgorithm) {
		for (Vertex destination : graph.getVertexes()) {
			LinkedList<Vertex> path = dijkstraAlgorithm.getPath(destination);
			if (path != null) {
				String cacheKey = buildCacheKey(graph.getName(), source.getName(), destination.getName());
				try {
					template.opsForHash().put(cacheKey, ROUTE_KEY, StringUtils.join(path, " "));
					template.opsForHash().put(cacheKey, DISTANCE_KEY, String.valueOf(dijkstraAlgorithm.getShortestDistance(destination)));
				} catch (Exception e) {
					LOGGER.warn("Exception persisting dijkstra results to the cache", e);
					return;
				}
			}
		}
	}
	
	@Override
	public String getBestRoute(String graphName, String sourceName, String destinationName) {
		String cacheKey = buildCacheKey(graphName, sourceName, destinationName);
		try {
			if (template.opsForHash().hasKey(cacheKey, ROUTE_KEY)) {
				return (String) template.opsForHash().get(cacheKey, ROUTE_KEY);
			}
		} catch (Exception e) {
			LOGGER.warn("Exception reading cache", e);
		}
		return null;
	}
	
	@Override
	public BigDecimal getBestRouteDistance(String graphName, String sourceName, String destinationName) {
		String cacheKey = buildCacheKey(graphName, sourceName, destinationName);
		try {
			if (template.opsForHash().hasKey(cacheKey, DISTANCE_KEY)) {
				return new BigDecimal((String) template.opsForHash().get(cacheKey, DISTANCE_KEY));
			}
		} catch (Exception e) {
			LOGGER.warn("Exception reading cache", e);
		}
		return null;
	}
	
	private String buildCacheKey(String graphName, String sourceName, String destinationName) {
		return graphName + "-" + sourceName + "-" + destinationName;
	}
}
