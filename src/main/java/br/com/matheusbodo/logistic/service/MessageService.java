package br.com.matheusbodo.logistic.service;

public interface MessageService {

	String getMessage(String messageKey);
}
