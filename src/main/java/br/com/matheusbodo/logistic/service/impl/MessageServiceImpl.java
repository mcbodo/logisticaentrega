package br.com.matheusbodo.logistic.service.impl;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import br.com.matheusbodo.logistic.service.MessageService;

@Service
public class MessageServiceImpl implements MessageService {
	
	@Autowired
	private MessageSource messageSource;

	@Override
	public String getMessage(String messageKey) {
		Locale locale = LocaleContextHolder.getLocale();
		return messageSource.getMessage(messageKey, null, locale);
	}

}
