package br.com.matheusbodo.logistic.controller;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.matheusbodo.logistic.dto.GraphDTO;
import br.com.matheusbodo.logistic.dto.Result;
import br.com.matheusbodo.logistic.entity.Graph;
import br.com.matheusbodo.logistic.service.GraphService;

@RestController
public class GraphController {
	
	@Autowired
	private GraphService graphService;

	@RequestMapping(value="/graphs", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createGraph(@RequestBody GraphDTO request) {
		if (!request.isValid()) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		Graph graph = request.buildGraph();
		Result<Graph> result = graphService.create(graph);
		if (result.isSuccess()) {
			try {
				return ResponseEntity.created(new URI("/graphs/" + graph.getId())).build();
			} catch (URISyntaxException e) {
				return ResponseEntity.created(null).build();
			}
		} else {
			return new ResponseEntity<Result<Graph>>(result, HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value="/graphs/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> find(@PathVariable("id") Long id) {
		Graph graph = graphService.findOne(id);
		if (graph != null) {
			GraphDTO response = new GraphDTO(graph);
			return ResponseEntity.ok(response);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@RequestMapping(value="/graphs/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable("id") Long id) {
		Result<Graph> result = graphService.delete(id);
		if (result.isSuccess()) {
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}
