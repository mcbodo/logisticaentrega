package br.com.matheusbodo.logistic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.matheusbodo.logistic.dto.BestRouteRequest;
import br.com.matheusbodo.logistic.dto.BestRouteResponse;
import br.com.matheusbodo.logistic.dto.Result;
import br.com.matheusbodo.logistic.service.GraphService;

@RestController
public class BestRouteController {
	
	@Autowired
	private GraphService graphService;

	@RequestMapping(path="/best-route/calculate", method=RequestMethod.POST, 
		consumes=MediaType.APPLICATION_JSON_VALUE, 
		produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> calculate(@RequestBody BestRouteRequest request) {
		if (!request.isValid()) {
			return ResponseEntity.badRequest().build();
		}
		Result<BestRouteResponse> result = graphService.calculateBestRoute(request);
		
		if (result.isSuccess()) {
			return ResponseEntity.ok(result.getObject());
		} else {
			return new ResponseEntity<Result<BestRouteResponse>>(result, HttpStatus.NOT_FOUND);
		}
	}
}
