package br.com.matheusbodo.logistic.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.matheusbodo.logistic.entity.Graph;

@Transactional
public interface GraphRepository extends JpaRepository<Graph, Long>{

	Graph findByName(String name);
}
