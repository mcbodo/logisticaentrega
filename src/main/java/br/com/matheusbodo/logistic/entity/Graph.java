package br.com.matheusbodo.logistic.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Graph implements Serializable {

	private static final long serialVersionUID = -2279428678892211176L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(unique=true)
	private String name;
	
	@OneToMany(mappedBy="graph", cascade=CascadeType.ALL, orphanRemoval=true)
	private final List<Vertex> vertexes = new LinkedList<>();
	
	@OneToMany(mappedBy="graph", cascade=CascadeType.ALL, orphanRemoval=true)
	private final List<Edge> edges = new LinkedList<>();

	public Graph() {
	}
	
	public long getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public List<Vertex> getVertexes() {
		return Collections.unmodifiableList(vertexes);
	}

	public List<Edge> getEdges() {
		return Collections.unmodifiableList(edges);
	}
	
	public void addVertex(Vertex vertex) {
		this.vertexes.add(vertex);
	}
	
	public void addEdge(Edge edge) {
		this.edges.add(edge);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Graph other = (Graph) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public void addRoute(String sourceName, String destinationName, BigDecimal distance) {
		Vertex source = getVertex(sourceName);
		if (source == null) {
			source = new Vertex(this, sourceName);
			vertexes.add(source);
		}
		
		Vertex destination = getVertex(destinationName);
		if (destination == null) {
			destination = new Vertex(this, destinationName);
			vertexes.add(destination);
		}
		
		Edge edge = new Edge(this, source, destination, distance);
		if (!edges.contains(edge)) {
			edges.add(edge);
		}
	}
	
	public Vertex getVertex(String name) {
		for (Vertex vertex : vertexes) {
			if (vertex.getName().equals(name)) {
				return vertex;
			}
		}
		return null;
	}

}