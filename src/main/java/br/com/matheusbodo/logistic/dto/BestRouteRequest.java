package br.com.matheusbodo.logistic.dto;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;

public class BestRouteRequest {

	private String graphName;
	
	private String sourceName;
	
	private String destinationName;
	
	private BigDecimal fuelConsumption;
	
	private BigDecimal fuelPrice;

	public String getGraphName() {
		return graphName;
	}

	public void setGraphName(String graphName) {
		this.graphName = graphName;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public BigDecimal getFuelConsumption() {
		return fuelConsumption;
	}

	public void setFuelConsumption(BigDecimal fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public BigDecimal getFuelPrice() {
		return fuelPrice;
	}

	public void setFuelPrice(BigDecimal fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destinationName == null) ? 0 : destinationName.hashCode());
		result = prime * result + ((fuelConsumption == null) ? 0 : fuelConsumption.hashCode());
		result = prime * result + ((fuelPrice == null) ? 0 : fuelPrice.hashCode());
		result = prime * result + ((graphName == null) ? 0 : graphName.hashCode());
		result = prime * result + ((sourceName == null) ? 0 : sourceName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BestRouteRequest other = (BestRouteRequest) obj;
		if (destinationName == null) {
			if (other.destinationName != null)
				return false;
		} else if (!destinationName.equals(other.destinationName))
			return false;
		if (fuelConsumption == null) {
			if (other.fuelConsumption != null)
				return false;
		} else if (!fuelConsumption.equals(other.fuelConsumption))
			return false;
		if (fuelPrice == null) {
			if (other.fuelPrice != null)
				return false;
		} else if (!fuelPrice.equals(other.fuelPrice))
			return false;
		if (graphName == null) {
			if (other.graphName != null)
				return false;
		} else if (!graphName.equals(other.graphName))
			return false;
		if (sourceName == null) {
			if (other.sourceName != null)
				return false;
		} else if (!sourceName.equals(other.sourceName))
			return false;
		return true;
	}

	public boolean isValid() {
		return StringUtils.isNotBlank(graphName) && StringUtils.isNotBlank(sourceName) &&
				StringUtils.isNotBlank(destinationName) && fuelConsumption != null &&
				fuelPrice != null;
	}
	
}
