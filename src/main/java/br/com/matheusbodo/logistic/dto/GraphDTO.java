package br.com.matheusbodo.logistic.dto;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Formatter;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.matheusbodo.logistic.entity.Edge;
import br.com.matheusbodo.logistic.entity.Graph;

public class GraphDTO {

	private Long id;
	private String name;
	private List<String> routes;
	
	public GraphDTO() {
		
	}
	
	public GraphDTO(Graph graph) {
		this.id = graph.getId();
		this.name = graph.getName();
		this.routes = new LinkedList<>();
		for (Edge edge : graph.getEdges()) {
			Formatter formatter = new Formatter();
			this.routes.add(formatter.format("%s %s %s", edge.getSource().getName(), edge.getDestination().getName(), edge.getWeight().toPlainString()).toString());
			formatter.close();
		}
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getRoutes() {
		if (routes != null) {
			return Collections.unmodifiableList(routes);
		}
		return null;
	}
	public void setRoutes(List<String> routes) {
		this.routes = routes;
	}
	
	public Graph buildGraph() {
		Graph graph = new Graph();
		graph.setName(name);
		
		if (routes != null) {
			for (String route : routes) {
				String[] splittedRoute = route.split(" ");
				String sourceName = splittedRoute[0];
				String destinationName = splittedRoute[1];
				BigDecimal distance = new BigDecimal(splittedRoute[2]).setScale(2);
				graph.addRoute(sourceName, destinationName, distance);
			}
		}
		return graph;
	}

	@JsonIgnore
	public boolean isValid() {
		return StringUtils.isNotBlank(name) && routes != null && routes.size() > 0;
	}
	
	
}
