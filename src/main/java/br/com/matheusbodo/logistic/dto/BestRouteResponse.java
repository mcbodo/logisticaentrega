package br.com.matheusbodo.logistic.dto;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;

public class BestRouteResponse {

	private String route;
	
	private BigDecimal cost;
	
	public void addVertex(String vertex) {
		if (StringUtils.isBlank(route)) {
			route = vertex;
		} else {
			route += " " + vertex;
		}
	}
	
	public void setRoute(String route) {
		this.route = route;
	}
	
	public String getRoute() {
		return route;
	}
	
	public BigDecimal getCost() {
		return cost;
	}
	
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
}
