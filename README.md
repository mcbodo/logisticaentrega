# Sistema de Logística #

## Descrição ##

Sistema utilizado para encontrar o caminho mais curto entre dois pontos de uma malha logística utilizando um webservice. 
O sistema permite também cadastrar malhas logísticas através de um webservice.

## Setup ##

Para iniciar a aplicação, basta executar os seguintes passos:

1. Compilar o projeto utilizando o Maven: mvn clean install na raiz do projeto
2. (Opcional) Iniciar o servidor do Redis para otimizar performance
3. Executar o jar gerado na pasta target: java -jar target/logistica-entrega-0.0.1-SNAPSHOT.jar


Obs: o banco de dados será criado gerando dois arquivos na raiz do projeto (logistic.mv.db e logistic.trace.db) 

## Como utilizar a API ##

### Criar uma malha logística ###

Enviar POST para a URL /graphs contendo um JSON com o nome e rotas da malha, como no exemplo abaixo:


```
#!json

{
  "name": "SP",
  "routes": [
    "A B 10",
	"B D 15",
	"A C 20",
	"C D 30",
	"B E 50",
	"D E 30"
  ]
}
```

Em caso de sucesso, o servidor retornará o HTTP status 201 e um header com o Location da malha logística. Esse Location poderá ser acessado para exibir o conteúdo da malha:

### Consultar uma malha logística cadastrada

Enviar GET para a URL /graphs/{id da malha}. Sistema retornará um JSON com o conteúdo, conforme exemplo abaixo:


```
#!json

{
    "id": 8,
    "name": "SP",
    "routes": [
        "A B 10",
        "B D 15",
        "A C 20",
        "C D 30",
        "B E 50",
        "D E 30"
    ]
}
```

### Remover uma malha logística cadastrada

Enviar DELETE para a URL /graphs/{id da malha}. O sistema irá retornar um HTTP status 200 em caso de sucesso.

## Calcular a melhor rota ###

Enviar POST para a URL /best-route/calculate com os dados necessários para calcular a rota. Os dados necessários são:

* graphName - nome da malha logística
* sourceName - nome do ponto de origem	
* destinationName - nome do ponto de destino
* fuelConsumption - autonomia do veículo
* fuelPrice - preço do combustível

Obs: os valores decimais deverão utilizar um ponto '.' como separador.

Exemplo de requisição:


```
#!json

{
  "graphName": "SP",
  "sourceName": "A",
  "destinationName": "D",
  "fuelConsumption": 10,
  "fuelPrice": 2.5 
}
```

Exemplo de resposta:


```
#!json

{
    "route": "A B D",
    "cost": 6.25
}
```

## Decisões de implementação ##

1. Caso seja enviado mais de uma veze a mesma rota (origem e destino), será considerada apenas a primeira. As outras serão ignoradas.
2. Optei por utilizar o banco de dados H2 para facilitar o setup da aplicação.


## Algoritmo de Dijkstra ##

A implementação do algoritmo de Dijkstra utilizada foi encontrada no site http://www.vogella.com/tutorials/JavaAlgorithmsDijkstra/article.html, por se tratar de uma implementação bastante didática. 

## Performance

Para otimizar a performance da aplicação, o resultado do cálculo do algoritmo de Dijkstra é armazenado em cache (Redis) toda vez que ele é executado. Antes de executar o algoritmo, o sistema consulta do cache se o resultado esperado já existe, evitando executá-lo novamente.

O uso do cache é opcional. Caso o servidor do Redis não seja encontrado o cálculo será realizado novamente em todas as consultas.

É recomendado que o Redis seja configurado para não persistir os dados, para evitar que ele contenha dados incorretos.